/**
 * Created by KUCKLU on 15/02/09.
 */
"use strict";

var gulp        = require('gulp'),
	$           = require('gulp-load-plugins')(),
	pngcrush    = require("imagemin-pngcrush"),
	browserSync = require('browser-sync'),
	reload      = browserSync.reload,
	runSequence = require('run-sequence'),
	fs          = require('fs'),
	rootPath    = __dirname.replace("/assets/src", ""),
	sources     = {
		html:         rootPath + '/**/*.html',
		php:          rootPath + '/**/*.php',
		ect:          ['ect/**/*.ect', '!' + 'ect/**/_*.ect'],
		scss:         'scss/**/*.scss',
		scssDir:      'scss/',
		css:          ['css/**/*.css', '!css/**/*.min.css'],
		cssDir:       'css/',
		cssDestDir:   '../css/',
		img:          'img/**/*.+(jpg|jpeg|png|gif|svg)',
		imgDestDir:   '../img/',
		font:         'font/**/*',
		fontDestDir:  '../font/',
		js:           ['js/**/*.js', '!js/**/*.min.js'],
		jsCopy:       'js/**/*.js',
		jsSrcDir:     'js/',
		jsDestDir:    '../js/',
		jsLib:      [
			'js/jquery-1.11.2.min.js',
			'js/jquery-migrate-1.2.1.min.js',
			'bower_components/modernizr/modernizr.js',
			'bower_components/fastclick/lib/fastclick.js',
			'bower_components/shufflejs/dist/jquery.shuffle.min.js',
			'bower_components/slick-carousel/slick/slick.min.js',
			'bower_components/foundation/js/foundation/foundation.js'
		],
		jsIE:       [
			'bower_components/html5shiv/dist/html5shiv.min.js',
			'bower_components/nwmatcher/src/nwmatcher.js',
			'bower_components/selectivizr/selectivizr.js',
			'bower_components/respond/dest/respond.min.js',
			'bower_components/REM-unit-polyfill/js/rem.min.js'
		]
	};

/*************************
 ******  HTML build ******
 *************************/
gulp.task("ect", function(){
	var json = JSON.parse(fs.readFileSync("ect.json"));

	return gulp.src(sources.ect)
		.pipe($.plumber())
		.pipe($.ect({data: function(filename, cb){
			console.log(filename);
			cb(json);
		}}))
		.pipe($.compressor({
			'preserve-line-breaks': true,
			'remove-intertag-spaces': true,
			'preserve-server-script': true,
			'preserve-php': true
		}))
		.pipe($.prettify({
			indent_size: 4,
		}))
		.pipe(gulp.dest(rootPath));
});

/**************************
 ******  Scss build  ******
 **************************/
gulp.task('scss', function(){
	return gulp.src(sources.scss)
		.pipe($.plumber())
		.pipe($.compass({
			config_file: 'config.rb',
			comments:     false,
			css:          sources.cssDir,
			sass:         sources.scssDir
		}))
		.pipe($.pleeease({
			autoprefixer: {browsers: ['last 4 versions']},
			opacity: true,
			filters: { 'oldIE': true },
			rem: true,
			mqpacker: true,
			import: true,
			pseudoElements: true,
			sourcemaps: false,
			next: false,
			minifier: false
		}))
		.pipe($.csscomb())
		.pipe(gulp.dest(sources.cssDestDir))
		.pipe($.rename({
			suffix: '.min',
			extname: '.css'
		}))
		.pipe($.pleeease({
			autoprefixer: false,
			minifier: true
		}))
		.pipe(gulp.dest(sources.cssDestDir))
		.pipe(reload({stream: true}));
});

/****************************
 ******  JS optimaize  ******
 ****************************/
gulp.task('jsLib', function(){
	return gulp.src(sources.jsLib)
		.pipe($.plumber())
		.pipe($.concat('libs.js'))
		.pipe(gulp.dest(sources.jsSrcDir))
		.pipe($.rename({suffix: '.min'}))
		.pipe($.uglify({preserveComments: 'some'}))
		.pipe(gulp.dest(sources.jsDestDir))
		.pipe(reload({stream: true, once: true}));
});
gulp.task('jsIE', function(){
    return gulp.src(sources.jsIE)
        .pipe($.plumber())
        .pipe($.concat('ie-libs.js'))
        .pipe(gulp.dest(sources.jsSrcDir))
        .pipe($.rename({suffix: '.min'}))
        .pipe($.uglify({preserveComments: 'some'}))
        .pipe(gulp.dest(sources.jsDestDir))
        .pipe(reload({stream: true, once: true}));
});
gulp.task('js', function(){
	return gulp.src(sources.js)
		.pipe($.plumber())
		.pipe($.rename({suffix: '.min'}))
		.pipe($.uglify({preserveComments: 'some'}))
		.pipe(gulp.dest(sources.jsDestDir))
		.pipe(reload({stream: true, once: true}));
});
gulp.task('jsCopy', function(){
	return gulp.src(sources.jsCopy)
		.pipe($.plumber())
		.pipe(gulp.dest(sources.jsDestDir));
});

/****************************
 ******  IMG optimaize ******
 ****************************/
gulp.task("img", function(){
	return gulp.src(sources.img)
		.pipe($.plumber())
		.pipe($.cache($.imagemin({
			optimizationLevel: 7,
			progressive: true,
			interlaced: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngcrush()]
		})))
		.pipe(gulp.dest(sources.imgDestDir))
		.pipe(reload({stream: true, once: true}));
});

/*******************
 ******  Font ******
 *******************/
gulp.task('font', function(){
	return gulp.src(sources.font)
		.pipe($.plumber())
		.pipe(gulp.dest(sources.fontDestDir))
		.pipe(reload({stream: true, once: true}));
});

/****************************
 ******  Browser sync  ******
 ****************************/
gulp.task('browserSync', function() {
	return browserSync.init(null, {
		server: {
			baseDir: rootPath + '/'
		},
		notify: false
	});
});
gulp.task('browserSyncReload', function() {
	return reload();
});

/***************************
 ******  Cache clear  ******
 ***************************/
gulp.task('clear', function ( i_done ) {
	return $.cache.clearAll( i_done );
});

/********************
 ******  Clean  *****
 ********************/
gulp.task('clean', $.shell.task(
	[
		'rm -rf ' + rootPath + '/*.html',
		'rm -rf ' + rootPath + '/assets/css/*',
		'rm -rf ' + rootPath + '/assets/js/*',
		'rm -rf ' + rootPath + '/assets/img/*',
		'rm -rf ' + rootPath + '/assets/src/css/*.css'
	]
));

/*********************
 ******  Delete  *****
 *********************/
gulp.task('delete', $.shell.task(
	[
		'rm -rf ' + rootPath + '/**/*/.sass-cache/',
		'rm -rf ' + rootPath + '/**/*/.gitkeep',
		'rm -rf ' + rootPath + '/*.ect',
		'rm -rf ' + rootPath + '/assets/src/css/*.css'
	]
));

/*********************
 ******  Build  ******
 *********************/
gulp.task('build', function(){
	return runSequence('clear', 'clean', ['ect', 'scss', 'jsLib', 'jsIE', 'js', 'img', 'font'], 'jsCopy', 'clear', 'delete' );
});

/*********************
 ******  Watch  ******
 *********************/
gulp.task('watch', function(){
	gulp.watch(['ect/**/*.ect','ect.json'], ['ect']);
	gulp.watch(sources.scss, ['scss']);
	gulp.watch(sources.jsLib, ['jsLib']);
	gulp.watch(sources.jsIE, ['jsIE']);
	gulp.watch(sources.js, ['js']);
	gulp.watch(sources.img, ['img']);
	gulp.watch(sources.font, ['font']);
	gulp.watch(sources.html, ['browserSyncReload']);
	gulp.watch(sources.php, ['browserSyncReload']);
});

/****************************
 ******  Default task  ******
 ****************************/
gulp.task('default', function(){
	return runSequence('clear', 'clean', ['ect', 'scss', 'jsLib', 'jsIE', 'js', 'img', 'font'], 'jsCopy', 'browserSync', 'watch');
});
