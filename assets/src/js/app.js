/*!
 * Created by KUCKLU on 15/02/10.
 */
jQuery(function($){
	'use strict';

	// Foundation
	$(document).foundation();

	// Slider
	$('.swipeSlide').slick({
		autoplay: true,
		autoplaySpeed: 5000,
		// fade: true,
		accessibility: false,
		// arrows: false,
	});
});
