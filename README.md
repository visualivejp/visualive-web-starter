VisuAlive Web Starter
======================

VisuAlive Web Starter は HTML コーディングをなるべく早く始められるようするためのモックアップです。 CSS フレームワークに Foundation を採用しています。

## 使用言語とビルドツール
HTML モックアップは CSS プリプロセッサの Sass、CSS フレームワークに Foundation、タスクランナーに Gulp を使用しています。  
予め Ruby、Sass、Compass、Node.js、Gulp、Bower をインストールしておいてください。  

> 参考サイト 1 : [初心者向け！CSSフレームワークFoundationのSass版を使用する為の環境構築。](http://designinglabo.com/388/sass_foundation.html)  
> 参考サイト 2 : [Mac OSX での開発環境構築](http://qiita.com/kuck1u/items/b95886b95faec4585305)

## モックアップのファイル編集
以下のコマンド実行後、ファイルの編集が可能になります。

```sh
$ cd /Users/YOURNAME/va-web-starter/assets/src
$ bower install
$ sudo npm install
$ gulp
```
※ Windows の場合は sudo は不要です。

## ディレクトリ構造

HTML モックアップの初期ディレクトリ構造です。  
※ assets/src/ 以下はお客様へ納品する時やウェブ公開時には不要です。

```
.
├── assets
│   ├── css
│   ├── font
│   ├── img
│   ├── js
│   └── src // HTML & PHP 以外のソース管理は基本このディレクトリで行う
│       ├── css
│       ├── ect
│       ├── font
│       ├── img
│       ├── js
│       ├── scss
│       │   ├── _settings.scss
│       │   └── style.scss
│       ├── bower.json
│       ├── config.rb
│       ├── csscomb.json
│       ├── ect.json
│       ├── gulpfile.js
│       └── package.json
├── .gitignore
├── README.md
└── index.html
```

## Gulp のタスク
* [ect] テンプレートエンジン EJS ファイルのビルド
* [scss] Scss ファイルのビルドとCSSファイルの圧縮
* [jsLib] 複数の JS ファイルを結合圧縮
* [jsIE] 複数の JS ファイルを結合圧縮
* [js] JS ファイルの圧縮
* [jsCopy] JS ファイルを `assets/src/js` ディレクトリから `assets/js` へコピーする
* [img] 画像ファイルの圧縮
* [font] フォントファイルを `assets/src/font` ディレクトリから `assets/font` へコピーする
* [browserSync] Browser-sync
* [browserSyncReload] Browser-sync
* [clear] キャッシュファイルの削除
* [clean] `/assets/css/`、`/assets/js/`、`/assets/img/`、`/assets/src/font/` を空にする
* [delete] `.sass-cache`、`.gitkeep` の削除と `/assets/src/css/` を空にする
* [build] clean、scss、ect、jsLib、jsIE、js、img、font、jsCopy、clear、deleteの順で行う
* [watch] Scss、JS、フォント、画像、HTML、PHP の監視
* [default] clean、scss、ect、jsLib、jsIE、js、img、font、jsCopy、browserSync、watch を行う

## 注意事項
`/*.html`、`/assets/css/`、`/assets/js/`、`/assets/img/`、`/assets/src/font/` は、Gulp 起動時並びに build タスク実行時に、一旦空になる事に注意。

## 更新履歴
* 2015 / 2 / 9 First commit。
